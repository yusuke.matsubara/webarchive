FROM ruby:3.2.2
ENV PKG_NAME webarchive
ENV APP_PATH /usr/src/myapp

WORKDIR $APP_PATH

# initial setup
COPY Gemfile Gemfile.lock *.gemspec ./
RUN mkdir -p lib/$PKG_NAME
COPY lib/$PKG_NAME/version.rb lib/$PKG_NAME/
RUN gem update --system

# install
RUN bundle config build.nokogiri --use-system-libraries
RUN bundle install
COPY . $APP_PATH
RUN bundle exec rake && bundle exec rake install
CMD bundle exec webarchive
