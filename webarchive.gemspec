# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'webarchive/version'

Gem::Specification.new do |spec|
  spec.name        = 'webarchive'
  spec.version     = WebArchive::VERSION
  spec.date        = '2019-06-30'
  spec.summary     = 'webarchive - CUI tool to archive URIs'
  spec.description = 'CUI tool to archive URIs using web.archive.org, archive.today, and others'
  spec.authors     = ['Yusuke Matsubara']
  spec.email       = 'whym@whym.org'
  spec.files       = ['lib/webarchive.rb', 'lib/webarchive/version.rb', 'README.md', 'LICENSE']
  spec.executables << 'webarchive'
  spec.require_paths = ['lib']
  spec.homepage    = 'https://rubygems.org/gems/webarchive'
  spec.license     = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    # spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

    spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['source_code_uri'] = 'https://gitlab.com/yusuke.matsubara/webarchive'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.required_ruby_version = '~> 3.0'

  spec.add_dependency 'addressable', '~> 2.8.0'
  spec.add_dependency 'concurrent-ruby', '~> 1.1.6'
  spec.add_dependency 'fast_trie', '~> 0.5.1'
  spec.add_dependency 'mechanize', '~> 2.8.0'
  spec.add_dependency 'net-http-persistent', '~> 4.0.0'
  spec.add_dependency 'simpleidn', '~> 0.2.1'

  spec.add_development_dependency 'bundler', '~> 2.1'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rspec', '~> 3.10'
  spec.add_development_dependency 'rubocop', '~> 0.81.0'
  spec.add_development_dependency 'webmock', '~> 3.13'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'pry-doc'
end
