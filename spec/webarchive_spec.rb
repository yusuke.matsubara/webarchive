# frozen_string_literal: true

require 'spec_helper'
require 'open3'
require 'webmock/rspec'

RSpec.describe WebArchive::ArchiveQueue do
  it 'calculates time required to maintain intervals' do
    q = WebArchive::ArchiveQueue.new('dummy', 20){}
    expect(q.time_until_next_req(1000, 1010)).to eq(10)
    expect(q.time_until_next_req(1000, 2000)).to eq(0)
    expect(q.time_until_next_req(1000, 0)).to eq(20)
  end

  it 'sends to dummy archive queue' do
    a = []
    EPS = 0.2
    q = WebArchive::ArchiveQueue.new('dummy', 0.0) do |uri|
      sleep EPS
      a << uri
    end
    q.enq WebArchive::Req.new('http://example.com', 0.0, 0)
    q.enq WebArchive::Req.new('http://example.com/2', 0.0, 0)
    expect(a).to eq([])
    sleep EPS * 2.5
    expect(a).to eq(['http://example.com', 'http://example.com/2'])
  end
end

RSpec.describe WebArchive::Client do
  it 'detects canonical uri' do
    stub_request(:get, 'http://www.example.com/').
      to_return(status: 200,
                body: '<head><link rel="canonical" href="http://foo.example.com/"/></head><body></body>',
                headers: { 'Content-Type' => 'text/html' })
    stub_request(:get, 'http://foo.example.com/').
      to_return(status: 200,
                body: '<body>foo</body>',
                headers: { 'Content-Type' => 'text/html' })
    client = WebArchive::Client.new
    expect(client.with_canonical_uri('http://www.example.com').then do |uri|
             URI.parse(uri).open
           end.wait).not_to(be_rejected)
    expect(WebMock).to have_requested(:get, 'www.example.com').once
    expect(WebMock).to have_requested(:get, 'foo.example.com').once
  end

  it 'detects relative canonical uri' do
    stub_request(:get, 'http://www.example.com/').
      to_return(status: 200,
                body: '<head><link rel="canonical" href="/main.html"/></head><body></body>',
                headers: { 'Content-Type' => 'text/html' })
    stub_request(:get, 'http://www.example.com/main.html').
      to_return(status: 200,
                body: '<body>foo</body>',
                headers: { 'Content-Type' => 'text/html' })
    client = WebArchive::Client.new
    expect(client.with_canonical_uri('http://www.example.com').then do |uri|
             URI.parse(uri).open
           end.wait).not_to(be_rejected)
    expect(WebMock).to have_requested(:get, 'www.example.com/main.html').once
  end

  it 'ignores same canonical uri' do
    stub_request(:get, 'http://www.example.com/').
      to_return(status: 200,
                body: '<head><link rel="canonical" href="http://www.example.com/"/></head><body></body>',
                headers: { 'Content-Type' => 'text/html' })

    client = WebArchive::Client.new
    client.with_canonical_uri('http://www.example.com').then do |uri|
      URI.parse(uri).open
    end.wait
    expect(WebMock).to have_requested(:get, 'www.example.com').once
  end

  it 'ignores same canonical uri, protocol relative' do
    stub_request(:get, 'http://www.example.com/').
      to_return(status: 200,
                body: '<head><link rel="canonical" href="//www.example.com/"/></head><body></body>',
                headers: { 'Content-Type' => 'text/html' })

    client = WebArchive::Client.new
    client.with_canonical_uri('http://www.example.com/').then do |uri|
      URI.parse(uri).open
    end.wait
    expect(WebMock).to have_requested(:get, 'www.example.com').once
  end

  it 'detects redirect' do
    stub_request(:get, 'http://www.example.com/').
      to_return(status: 301, body: 'forwarded',
                headers: { 'Location' => 'http://foo.example.com' })
    stub_request(:get, 'http://foo.example.com/').
      to_return(status: 200, body: '<body>foo</body>',
                headers: { 'Content-Type' => 'text/html' })
    client = WebArchive::Client.new
    client.with_canonical_uri('http://www.example.com').then do |uri|
      URI.parse(uri).open
    end.wait
    expect(WebMock).to have_requested(:get, 'www.example.com').once
    expect(WebMock).to have_requested(:get, 'foo.example.com').once
  end

  it 'detects no redirect' do
    stub_request(:get, 'http://www.example.com/').
      to_return(status: 200, body: 'not forwarded', headers: {})
    client = WebArchive::Client.new
    client.with_canonical_uri('http://www.example.com').then do |uri|
      URI.parse(uri).open
    end.wait
    expect(WebMock).to have_requested(:get, 'www.example.com').once
  end

  it 'ignores 404' do
    stub_request(:get, 'http://www.example.com/').
      to_return(status: 404, headers: {})
    client = WebArchive::Client.new
    expect { client.with_canonical_uri('http://www.example.com').wait }
      .not_to(raise_error)
    expect(client.with_canonical_uri('http://www.example.com').wait)
      .to be_rejected

    expect { client.with_redirect('http://www.example.com').wait }
      .not_to raise_error
    expect(client.with_redirect('http://www.example.com').wait)
      .to be_rejected
  end

  it 'ignores 503' do
    stub_request(:get, 'http://www.example.com/').
      to_return(status: 503, headers: {})
    client = WebArchive::Client.new
    expect { client.with_canonical_uri('http://www.example.com') }
      .not_to raise_error
    expect(client.with_canonical_uri('http://www.example.com').wait)
      .to be_rejected
    expect { client.with_redirect('http://www.example.com') }
      .not_to raise_error
    expect(client.with_redirect('http://www.example.com').wait)
      .to be_rejected
  end
end

RSpec.describe WebArchive.method(:to_ascii_uri) do
  it 'normalizes i18n domain to ascii URI' do
    expect(WebArchive.to_ascii_uri("https://www.\u9EDE\u770B.com").to_s)
      .to eq('https://www.xn--c1yn36f.com')
  end

  it 'percent-encodes' do
    expect(WebArchive.to_ascii_uri("https://ja.wikipedia.org/wiki/\u535A\u58EB\uFF08\u6CD5\u5B66\uFF09").to_s)
      .to eq('https://ja.wikipedia.org/wiki/%E5%8D%9A%E5%A3%AB%EF%BC%88%E6%B3%95%E5%AD%A6%EF%BC%89')
  end

  it 'retains percent-encoded string' do
    u = 'https://ja.wikipedia.org/wiki/%E5%8D%9A%E5%A3%AB%EF%BC%88%E6%B3%95%E5%AD%A6%EF%BC%89'
    expect(WebArchive.to_ascii_uri(u).to_s).to eq(u)
  end

  it 'retains key-value parameters' do
    u = 'https://ja.wikipedia.org/w/index.php?title=Foo&oldid=10'
    expect(WebArchive.to_ascii_uri(u).to_s).to eq(u)
  end
end

RSpec.describe WebArchive.method(:launch) do
  def my_tempfile_path
    tf = Tempfile.open('webarchive')
    path = tf.path
    tf.close(true)
    path
  end

  it 'creates history file' do
    path = my_tempfile_path
    allow(File).
      to receive(:expand_path).with(WebArchive::HISTORY_FILE).and_return(path)
    allow(Readline).to receive(:readline).and_return(nil)
    WebArchive.launch(history: true)
    expect(File.exist?(path)).to be true
    expect(File.read(path)).to eq('')
  end

  it 'does not create history file when told not to' do
    path = my_tempfile_path
    allow(File).
      to receive(:expand_path).with(WebArchive::HISTORY_FILE).and_return(path)
    allow(Readline).to receive(:readline).and_return(nil)
    WebArchive.launch(history: false)
    expect(File.exist?(path)).to be false
  end

  it 'ends for non uri' do
    allow(Readline).to receive(:readline).and_return('Non uri', nil)
    expect { WebArchive.launch(history: false) }.
      to output("invalid; skipping 'Non uri'\n").to_stderr
  end

  it 'exits safely for unreachable input' do
    WebMock.allow_net_connect!
    stub_request(:get, %r{https://web.archive.org/save/.*}).
      to_return(status: 500, headers: {})
    stub_request(:get, %r{https://archive.today/.*}).
      to_return(status: 500, headers: {})
    stub_request(:get, %r{https://megalodon.jp/.*}).
      to_return(status: 500, headers: {})
    allow(Readline).
      to receive(:readline).and_return('https://www.example.invalid', nil)
    expect { WebArchive.launch(history: false) }.not_to raise_error
  end

  it 'make 2 retries for "too many requests" error (429)' do
    WebMock.allow_net_connect!
    wao_stub = stub_request(
      :get, %r{https://web.archive.org/save}
    ).to_return(
      status: 429, body: '<form method="POST" id="web-save-form"><input type="text" name="url"/></form>',
      headers: { 'Content-Type' => 'text/html' }
    )
    stub_request(:get, %r{https://archive.today/}).
      to_return(status: 200, body: '<body>foo</body>',
                headers: { 'Content-Type' => 'text/html' })
    stub_request(:get, %r{https://megalodon.jp/pc/?.*}).
      to_return(status: 200, body: '<body>foo</body>',
                headers: { 'Content-Type' => 'text/html' })
    allow(Readline).
      to receive(:readline).and_return('https://www.example.com', nil)
    WebArchive.launch(history: false, wait_secs: 0.01, max_retry: 2,
                      canonical_uri: false, redirect: false)

    expect(wao_stub).to have_been_requested.times(3)
  end
end

RSpec.describe WebArchive do
  it 'has a version number' do
    expect(WebArchive::VERSION).not_to be nil
  end

  it 'defines warn_archive_fail' do
    expect(defined? WebArchive.warn_archive_fail).to eq('method')
  end

  def with_env_and_bin
    bin = File.join([MY_GEM_ROOT, 'bin/webarchive'])
    env = { 'RUBYLIB' => File.join([MY_GEM_ROOT.path, 'lib']) }
    yield env, bin
  end

  context 'executable' do
    it 'has a working executable, prints version' do
      with_env_and_bin do |env, bin|
        expect(File.exist?(bin)).to be true
        expect(File.executable?(bin)).to be true
        Open3.popen3(env, bin, '--version') do |_, o, e, t|
          expect(o.read.strip).to eq("webarchive #{WebArchive::VERSION}")
          expect(e.read.strip).to eq('')
          expect(t.value).to eq(0)
        end
      end
    end

    it 'shows help' do
      with_env_and_bin do |env, bin|
        Open3.popen3(env, bin, '-h') do |_, o, e, t|
          expect(o.read.strip).to start_with("Usage: webarchive [options]")
          expect(e.read.strip).to eq('')
          expect(t.value).to eq(0)
        end
      end
    end

    it 'gives prompt and exits properly for an empty input' do
      with_env_and_bin do |env, bin|
        Open3.popen3(env, bin, '--no-history') do |i, o, e, t|
          i.close
          expect(o.read.strip).to eq('Q(0)>')
          expect(e.read.strip).to eq('')
          expect(t.value).to eq(0), e.read
        end
      end
    end
  end
end
