#! /usr/bin/env ruby
# frozen_string_literal: true

# This tool allows you to call multiple web archiving services. It
# works from command-line interactively. In the inside, it maintains a
# throttled queue for each service provider supported.

# (This file itself is executable without installation - see the bottom.)

require 'open-uri'
require 'readline'
require 'tempfile'
require 'simpleidn'
require 'net/http'
require 'addressable/uri'
require 'digest/sha2'
require 'mechanize'
require 'trie'
require 'concurrent'

# classes and functions of webarchive package
module WebArchive
  Req = Struct.new("Req", :uri, :wait, :max_retry)

  # error class for unexpected response from archiving service
  class UnexpectedResponseError < StandardError
    def initialize(cause = nil)
      super(cause)
    end
  end

  # error class for when redirect/canonical uri is not found
  class NoAlternativeURIError < StandardError
    def initialize(cause = nil)
      super(cause)
    end
  end

  begin
    require 'libnotify'
    def self.warn_archive_fail(req, archiver, body)
      warn "Not archived: #{req} by #{archiver}; #{body}"
      Libnotify.show(summary: "Not archived: #{req} by #{archiver}",
                     body: body, timeout: 3)
    end
  rescue LoadError
    unless respond_to? :warn_archive_fail
      def self.warn_archive_fail(req, archiver, body)
        warn "Not archived: #{req} by #{archiver}; #{body}"
      end
    end
  end

  # Queue for sending URLs to a certain archiving web site
  # The block given to constructor will be executed for each '<<'
  class ArchiveQueue < Queue
    # Create a new instance of ArchiveQueue
    # @param name [String]  name of the queue
    # @param interval [Float]   length of the wait between requests
    # @yield [String] URI that the queue receives
    def initialize(name, interval)
      super()
      @name = name
      @interval = interval
      @all_sent = false
      @in_process = Concurrent::AtomicFixnum.new(0)
      last_request_time = Time.now - interval
      @consumer = Thread.new do
        loop do
          req = self.deq        # deq blocks until non-empty
          @in_process.value += 1
          begin
            sleep time_until_next_req(last_request_time, Time.now)
            last_request_time = Time.now
            yield req.uri
          rescue StandardError => e
            if retry?(e) && req.max_retry.positive?
              buff = [].tap { |a| a << self.deq until self.empty? }
              @in_process.value += buff.size + 1
              Concurrent::ScheduledTask.execute(req.wait) do
                @in_process.value -= buff.size + 1
                self.enq Req.new(req.uri, req.wait * 2, req.max_retry - 1)
                buff.each { |x| self.enq x }
              end
            else
              WebArchive.warn_archive_fail(
                req.uri, name, ([e.inspect] + e.backtrace).join("\n")
              )
            end
          ensure
            @in_process.value -= 1
            break if @all_sent && self.remaining.zero?
          end
        end
      end
    end

    def time_until_next_req(last_req, current)
      elapsed = [current - last_req, 0].max
      [@interval - elapsed, 0].max
    end

    # @param exc [Exception]
    # @return [Boolean]
    def retry?(exc)
      [
        Errno::ECONNRESET,
        Errno::EHOSTUNREACH
      ].include?(exc.class) ||
        (exc.is_a?(OpenURI::HTTPError) && exc.message.start_with?('429 ')) ||
        (exc.is_a?(OpenURI::HTTPError) && exc.message.start_with?('502 ')) ||
        (exc.is_a?(OpenURI::HTTPError) && exc.message.start_with?('503 ')) ||
        (exc.is_a?(Mechanize::ResponseCodeError) && exc.response_code == '503')
    end

    # mark as 'sending done' and wait for items to be processed
    # @return [Boolean]
    def done_sending
      @all_sent = true
      @consumer.join if self.remaining.positive?
    end

    # number of queued items (including those being processed)
    # @return [Integer]
    def remaining
      self.size + @in_process.value
    end
  end

  # Client with multiple queues
  class Client
    def initialize(wait_secs: 1, max_retry: 3,
                   redirect: false, canonical_uri: true)
      @wait_secs = wait_secs
      @max_retry = max_retry
      @redirect = redirect
      @canonical_uri = canonical_uri

      @wait_secs = 0 if @wait_secs.negative?
      @max_retry = 0 if @max_retry.negative?
      @queues = []
    end

    # @param queue [ArchiveQueue]
    def add_queue(queue)
      @queues << queue
    end

    def queued_uris
      @queues.map(&:remaining).inject(:+)
    end

    # @param uri [String]
    # @return [Concurrent::Promises::Future] Gives the target URI if redirected
    def with_redirect(uri)
      Concurrent::Promises.future do
        res = Net::HTTP.get_response(Addressable::URI.parse(uri))
        raise NoAlternativeURIError, 'no redirect found' if
          !res['location'] || res['location'] == uri

        res['location']
      end
    end

    def add_scheme(uri, scheme)
      if uri.relative?
        uri = uri.dup
        uri.scheme = scheme
      end
      uri
    end

    def equivalent_uri?(uri, str)
      uri = add_scheme(uri, Addressable::URI.parse(str).scheme)
      uri.to_s == str
    end

    # @param uri [String]
    # @return [Concurrent::Promises::Future] Gives the canonical URI if there is one
    def with_canonical_uri(uri)
      Concurrent::Promises.future do
        agent = Mechanize.new
        page = agent.get(uri)
        ret = nil
        raise NoAlternativeURIError, 'no canonical URI found' unless
          page.canonical_uri &&
          page.class == Mechanize::Page &&
          page.canonical_uri != page.uri

        if page.canonical_uri.relative?
          u2 = URI.join(page.uri, page.canonical_uri)
          ret = u2.to_s if !equivalent_uri?(u2, uri) &&
                           !equivalent_uri?(u2, page.uri)
        else
          u1 = page.canonical_uri
          u1 = add_scheme(u1, 'http') unless u1.scheme
          ret = u1.to_s if !equivalent_uri?(u1, uri) &&
                           !equivalent_uri?(u1, page.uri)
        end

        raise NoAlternativeURIError, 'no canonical URI found' unless ret

        ret
      end
    end

    # @param uri [String]
    # @return [void]
    def send_single_uri(uri)
      @queues.each do |q|
        q.enq Req.new(uri, @wait_secs, @max_retry)
      end
    end

    # @param uri [String]
    # @return [Concurrent::Promises::Future]
    def send_uri(uri)
      f0 = Concurrent::Promises.future{ send_single_uri(uri) }
      f1 = with_canonical_uri(uri).then { |x| send_single_uri(x) } if @canonical_uri
      f2 = with_redirect(uri).then { |x| send_single_uri(x) } if @redirect
      f1 ||= Concurrent::Promises.future{}
      f2 ||= Concurrent::Promises.future{}
      f0.zip(f1).zip(f2)
    end

    def wait_for_queues
      @queues.each(&:done_sending)
    end
  end

  # @param str [String]
  # @return [String]
  def self.encode_non_ascii(str)
    if str =~ /[^[:ascii:]]/
      Addressable::URI.encode(str)
    else
      str
    end
  end

  # @param str [String]
  # @return [String]
  def self.prepend_http(uri)
    if %r/\.[a-z]{2,4}(\/|$)/.match(uri) && %r{(^http|://)}.match(uri).nil?
      'http://' + uri
    else
      uri
    end
  end

  # Encode non-ASCII components in the given string and make a URI instance from
  # @param str [String]
  # @return [Addressable::URI]
  def self.to_ascii_uri(str)
    uri = prepend_http(str.strip)
    u = Addressable::URI.parse(uri)
    u.host = SimpleIDN.to_ascii(u.host)
    u.path, u.query, u.fragment = [
      u.path, u.query, u.fragment
    ].map(&method(:encode_non_ascii))
    u
  end

  # Write log to a file
  # @param source [String]
  # @param content [String]
  # @return [void]
  def self.debug_output(source, uri, content)
    ts = Time.now.strftime('%Y%m%d%H%M%S')
    filename = "#{self}-#{source}-#{uri.gsub(/\W+/, '_')[0..30]}-"
    filename += Digest::SHA256.hexdigest(uri + ts)[0..8]
    Tempfile.open(filename) do |f|
      f.puts content
    end
  end

  # completer for URLs
  class Completer
    # @param history_file [File]
    def initialize(history_file)
      @file = history_file
      @trie = Trie.new
      reload!
    end

    # @return [void]
    def update!
      reload! if File.stat(@file).mtime > @lastupdate
    end

    # @return [void]
    def reload!
      if File.exist? @file
        File.open(@file, encoding: 'utf-8').each_line do |x|
          @trie.add x.strip
        end
      else
        File.new(@file, 'w', encoding: 'utf-8').flush
      end
      @lastupdate = Time.now
    end

    # @return [Proc]
    def to_proc
      proc do |s|
        update!
        @trie.children(s)
      end
    end

    # @param str [String]
    # @return [void]
    def append_to_history(str)
      File.open(@file, mode: 'a', encoding: 'utf-8') do |f|
        f.puts str
      end
    end
  end

  HISTORY_FILE = '~/.webarchive_history'

  # Launch the CLI
  # @return [Concurrent::Promises::Future]
  def self.launch(wait_secs: 1, max_retry: 3,
                  redirect: false, canonical_uri: true,
                  history: true, debug: false, verbose: false)
    wait_secs = 0 if wait_secs.negative?
    max_retry = 0 if max_retry.negative?

    Thread.abort_on_exception = true
    completer = nil
    if history
      completer = Completer.new(File.expand_path(HISTORY_FILE))
      Readline.completion_proc = completer.to_proc
      Readline.completion_append_character = ''
    end

    client = Client.new(wait_secs: wait_secs, max_retry: max_retry,
                        redirect: redirect, canonical_uri: canonical_uri)

    # prepare queues
    client.add_queue(
      ArchiveQueue.new('archive.org (logged out)', wait_secs) do |uri|
        URI.parse('https://web.archive.org/save/' + uri).open do |f|
          if f.meta['content-location'] && verbose
            puts "<https://web.archive.org#{f.meta['content-location']}>"
          elsif verbose
            puts f.meta.inspect
          end
        end
      end
    )

    client.add_queue(
      ArchiveQueue.new('megalodon.jp', wait_secs) do |uri|
        agent = Mechanize.new
        page = agent.get('https://megalodon.jp/pc/?' +
                         Addressable::URI.form_encode(url: uri))
        form = page.forms.first
        raise UnexpectedResponseError, page.inspect unless form

        res = agent.submit(form)
        if debug
          debug_output('megalodonjp', uri, res.body)
        end
        og = res.at('meta[property="og:url"]')
        uri = if og
                og[:content]
              else
                res.links.map(&:href).find(-> { res.uri.to_s }) do |x|
                  x =~ %r{megalodon\.jp/[\d-]+/}
                end
              end
        puts "<#{uri}>" if verbose
        agent.shutdown
      end
    )

    client.add_queue(
      ArchiveQueue.new('archive.today', wait_secs) do |uri|
        agent = Mechanize.new
        agent.follow_meta_refresh = true

        page = agent.get('https://archive.is/')
        form = page.form_with(id: 'submiturl')
        if debug
          debug_output('archivetoday', uri, page.inspect)
        end
        raise UnexpectedResponseError, page.inspect unless form

        form['anyway'] = '1'
        form.field_with(name: 'url').value = uri
        sleep 5.0               # not submit too fast
        page = agent.submit(form)
        puts "<#{page.uri}>" if verbose
        agent.shutdown
      end
    )

    # main loop

    uri_regexp = URI::DEFAULT_PARSER.make_regexp
    all = Concurrent::Promises.future{}
    while line = Readline.readline("Q(#{client.queued_uris})> ", add_hist: true)
      uri = ''
      begin
        uri = to_ascii_uri(line).to_s
      rescue Addressable::URI::InvalidURIError => e
        warn_archive_fail(line.strip, '<>', e.message)
      end
      next if uri == ''

      puts uri if verbose

      if uri !~ uri_regexp
        warn "invalid; skipping '#{uri}'"
        next
      end

      f = client.send_uri(uri).then {
        completer&.append_to_history(uri)
      }.on_rejection { |reason, _|
        warn "skipping canonical/redirect for #{uri}: #{reason}" if
          !x.is_a?(NoAlternativeURIError)
      }
      all = all.zip(f)
    end

    all.wait
    client.wait_for_queues
    # TODO: trap INT and ask for confirmation
    all
  end
end

if $PROGRAM_NAME == __FILE__
  WebArchive.launch(wait_secs: 1.0, verbose: true, debug: true)
end
